<?php 

$parent_directory = 'demo';
$file_types = 'jpg,png,gif,tif';

//===================================================//
// FUNCTION: directoryToArray                        //
//                                                   //
// Parameters:                                       //
//  - $root: The directory to process                //
//  - $to_return: f=files, d=directories, b=both     //
//  - $file_types: the extensions of file types to   //
//                 to return if files selected       //
//===================================================//
function directoryToArray($root, $to_return='b', $file_types=false) {
  $array_items = array();
  if ($file_types) { $file_types=explode(',',$file_types); }
  if ($handle = opendir($root)) {
    while (false !== ($file = readdir($handle))) {
      if ($file != "." && $file != "..") {

        $add_item = false;
        $type = (is_dir($root. "/" . $file))?'d':'f';
        $name = preg_replace("/\/\//si", "/", $file);

        if ($type=='d' && ($to_return=='b' || $to_return=='d') ) {
          $add_item = true;
        }

        if ($type=='f' && ($to_return=='b' || $to_return=='f') ) {
          $ext = end(explode('.',$name));
          if ( !$file_types || in_array($ext, $file_types) ) {
            $add_item = true;
          }
        }

        if ($add_item) {
          $array_items[] = array ( 'name'=>$name, 'type'=>$type, 'root'=>$root);
        }
      }
    } // End While
    closedir($handle);
  } // End If
  return $array_items;
}



if (isset($_POST[pickfile])) {

  // User has selected a file take whatever action you want based
  // upon the values for folder and file

} else {

    echo '
<html>
<head>
  <script type="text/javascript">
    function changeFolder(folder) {
      document.pickFile.submit();
    }
  </script>
</head>

<body>';


echo "<form name=\"pickFile\" method=\"POST\">\n";

$directoryList = directoryToArray($parent_directory,'d');

echo "<select name=\"folder\" onchange=\"changeFolder(this.value);\">\n";
foreach ($directoryList as $folder) {
  $selected = ($_POST[folder]==$folder[name])? 'selected' : '';
  echo "<option value=\"$folder[name]\" $selected>$folder[name]</option>\n"; 
}
echo '</select><br><br>';

$working_folder = ($_POST[folder]) ? $_POST[folder] : $directoryList[0][name];

$fileList = directoryToArray($parent_directory.'/'.$working_folder,'f',$file_types);

echo "<select name=\"file\">\n";
foreach ($fileList as $file) {
  echo "<option value=\"$file[name]\">$file[name]</option>\n"; 
}
echo '</select><br><br>';

echo "<button type=\"submit\" name=\"pickfile\">Submit</button>\n";

echo "</form>\n";
echo "</body>\n";
echo "</html>\n";

}
?>
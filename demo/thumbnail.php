<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta content="charset=utf-8">
		<title> FlexSlider 2 </title>
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demo.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="../flexslider.css" type="text/css" media="screen" />

		<!-- Modernizr -->
		<script src="js/modernizr.js"></script>

	</head>
	<body class="loading">

		<div id="container" class="cf">
			<?php
			//the image directory
			$imagedir = 'images';
			$myimages = array();
			if ($handle = opendir($imagedir)) {
				while (false !== ($file = readdir($handle))) {
					if ($file != "." && $file != "..") {

						$filepath = $imagedir . "/" . $file;

						array_push($myimages, $filepath);
					}
				}
				closedir($handle);
			}
			?>

			<div id="main" role="main">
				<section class="slider">
					<div id="slider" class="flexslider">
						<ul class="slides">
							<?php
								foreach ($myimages as $imagepath) {
							?>
							<li>
								<img src="<?php	echo $imagepath;?>" />
							</li>
							<?php
							}
							?>
						</ul>
					</div>
					<div id="carousel" class="flexslider">
						<ul class="slides">
						<?php
								foreach ($myimages as $imagepath) {
							?>
							<li>
								<img src="<?php	echo $imagepath;?>" />
							</li>
							<?php
							}
							?>
						</ul>
					</div>
				</section>

			</div>
		</div>

		<!-- jQuery -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>


		<!-- FlexSlider -->
		<script defer src="../jquery.flexslider.js"></script>

		<script type="text/javascript">
			$(function() {
				SyntaxHighlighter.all();
			});
			$(window).load(function() {
				$('#carousel').flexslider({
					animation : "slide",
					controlNav : false,
					animationLoop : false,
					slideshow : false,
					itemWidth : 110,
					itemMargin : 5,
					asNavFor : '#slider'
				});

				$('#slider').flexslider({
					animation : "slide",
					controlNav : false,
					animationLoop : false,
					slideshow : true,
					sync : "#carousel",
					start : function(slider) {
					$('body').removeClass('loading');
					}
				});
			});
		</script>	
	</body>
</html>